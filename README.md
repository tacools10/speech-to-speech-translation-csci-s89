# Speech-to-Speech-Translation-CSCI-S89

## Speech-to-text
The speech-to-text component of the model is an implementation of DeepSpeech 2 created by
Sean Naren. We use the [PyTorch implementation](https://github.com/SeanNaren/deepspeech.pytorch).
The DeepSpeech 2 model was originally created by a team of researchers at Baidu. You can find
the original paper [here](https://arxiv.org/pdf/1512.02595.pdf). 

The network has two convolutional layers, five bidrectional recurrent layers, and
a fully-connected layer. The loss function used is the connectionist temporal
classification loss function (CTC). 

### Dataset
If you click on the `deepspeech.py` folder in the repository, there is a separate
`README.md` that has information on what datasets are available. For our 
training, we used the LibriSpeech ASR corpus. It is a large-scale corpus with 
1000 hours of read English speech from audiobooks in the LibriVox project. The audio is sampled 
at 16khz and the corpus was originally prepared by Vassil Panayotov with help from Daniel Povey.

### To train
See insructions on training in the `deepspeech.py` folder `README.md`. 

### Inputs / Outputs
The model takes as input a `manifest.csv` file that has lists of `.txt` transcription
files and `.wav` audio files. It outputs a predicted transcription of the data.

## Neural Machine Translation
The neural machine translation portion uses OpenNMT, initially created by the Harvard NLP group and SYSTRAN.
We use the [PyTorch implementation](https://github.com/OpenNMT/OpenNMT-py) of it.

The neural network consists (mainly) of three LSTM layers, modified to add Attention.

### Dataset
We used the [Opus dataset](http://opus.nlpl.eu/Wikipedia.php), which gives English and Spanish sentence pairs for the same wikipedia article.
The dataset is 600Mb's large (2,000,000 sentence pairs) and for realistic training, we have used only 10% of the corpus.

### To train: 
Pre-process data:
`python preprocess.py -train_src project/eng_train.txt -train_tgt project/spa_train.txt 
-valid_src project/eng_val.txt -valid_tgt project/spa_val.txt -save_data project/data`

Train: 
`python3 train.py -data project/data -save_model eng_spa_model
-world_size 1 -gpu_ranks 0 -train_steps 100000 -valid_steps 10000`

`World-size` refers to the number of GPU's, and `gpu-rank` is which ones you want to use (in this case just the first one).
We trained our model for 100,000 steps with validation every 10,000 steps. 

### Inputs / Outputs
The model takes as input a line-delimited `.txt` file with sentences. Output is a line-delimited
`.txt` file with translations as well as `.mp3` files of speech in the target language, Spanish 
in our case.

### Tranlsation
The model is already trained, with file name eng2spa.pt.

`python translate.py -model eng2spa.pt -src project/to_translate.txt
-output predidction.txt -replace_unk -verbose`

## Text to Speech synthesis
For TTS, we used the Google TTS library, which is the basis for the TTS system in Google Translate.
TTS is built into the NMT translator, so each line of translation fed into the translator will output its own .mp3 file. 